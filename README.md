GitLab入門 (1) 入門
https://qiita.com/kaizen_nagoya/items/e8b9f4f0619e317db862

# Hello World

This is a mirror of the GitHub hello-world project.
https://github.com/kaizen-nagoya/hello-world/

Or that is the mirror of this project.

GitHub入門 (1) 入門
https://qiita.com/kaizen_nagoya/items/b6f5b1532fd21fec79c8

と似たことをGitLabでやりながら、それぞれのシステムの特徴を理解し、
使い勝手のいい方法を探す。

大きく違うのは、グループかも。最初にグループを作るか、最初にプロジェクトを作るか。

#口座(account)開設

Google, Twitter, Github, Bitbucket, SalesforceのIDがあれば、

![gitlab0.png](https://qiita-image-store.s3.ap-northeast-1.amazonaws.com/0/51423/0fc24ef0-d45a-2c2f-8091-8db2a7c5bba7.png)

なければ、「sign in」の横の「Register」(登録)を撰択。
![gitlab0a.png](https://qiita-image-store.s3.ap-northeast-1.amazonaws.com/0/51423/1fd5c6bf-4e92-126a-b2da-3f5374d4e9e9.png)

もくもくと入力してください。


#グループの作成

画面の左上の「グループ」
を押すと、右上の方に

「新規グループ」というボタンが現れる。
![gitlab1.png](https://qiita-image-store.s3.ap-northeast-1.amazonaws.com/0/51423/688c7a89-e5cf-88ff-ad25-7aa2a01d4c85.png)


helloworld_gl

という名前で、公開のグループを作成した。

https://gitlab.com/helloworld_gl/

![gitlab2.png](https://qiita-image-store.s3.ap-northeast-1.amazonaws.com/0/51423/5b04c516-829c-ef40-eaca-88abbbb248d5.png)

![gitlab3.png](https://qiita-image-store.s3.ap-northeast-1.amazonaws.com/0/51423/3cabc1e5-5acd-73d0-b2ac-c2f2c4d96eea.png)


https://gitlab.com/helloworld_gl/hello-world

# プロジェクトの作成

グループを作らずに、プロジェクトを作ることができる。
>Want to house several dependent projects under the same namespace? Create a group.

![gitlab1a.png](https://qiita-image-store.s3.ap-northeast-1.amazonaws.com/0/51423/2b048732-fc8f-3050-11a1-dc1e0ca08aaa.png)


![gitlaba2.png](https://qiita-image-store.s3.ap-northeast-1.amazonaws.com/0/51423/c6fd455f-ff56-519a-f58c-0848054372da.png)

#現在地点

いまここ
グループから作った方。
https://gitlab.com/helloworld_gl/hello-world

プロジェクトから作った方。
https://gitlab.com/kaizen_nagoya/helloworld_gl2